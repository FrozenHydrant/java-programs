/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notdesmond.richard.HelloWorld.src.helloworld;

/**
 *
 * @author ricky
 */
public class FirstTwentyEvenProduct {

    public static void main(String[] args) {
        long product = 1;
        for (int i = 2; i <= 20; i = i + 2) {
            product = product * i;
        }
        System.out.println(product);

        product = 1;
        for (int i = 2; i <= 20; i++) {
            if (i % 2 == 0) {
                product = product * i;
            }
        }
        System.out.println(product);
    }
}
