/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notdesmond.richard.HelloWorld.src.helloworld;

/**
 *
 * @author ricky
 */
public class HelloWorld {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("Hello World!");

        int i = 5 / 2;
        System.out.println(i);

        i = 9;
        System.out.println(i % 2);
        i = 99;
        System.out.println(i % 3);
        System.out.println(i % 4);

        int j;
        j = i % 4;
        System.out.println(j);

        double pie = 3.14;
        int r = 10;
        double area = pie * r * r;
        System.out.println(area);

        int x = 5;
        int y = 3;
        if (x < y) {
            System.out.println("X is smaller.");
        } else {
            System.out.println("X might be bigger");
        }

    }

}
