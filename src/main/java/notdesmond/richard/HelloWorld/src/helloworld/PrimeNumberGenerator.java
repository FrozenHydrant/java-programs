package notdesmond.richard.HelloWorld.src.helloworld;

public class PrimeNumberGenerator {
    public static void main(String args[]) {
        boolean isPrime = true;
        for (int i = 1; i < 10000; i++) {
            isPrime = true;
            for (int j = 2; j < i; j++) {
                if (i % j == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) {
                System.out.println(i);
            }
        }
    }
}