/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notdesmond.richard.HelloWorld.src.helloworld;

/**
 *
 * @author ricky
 */
public class TenFactorial {

    public static void main(String[] args) {
        int product = 1;
        for (int i = 1; i <= 10; i++) {
            product = product * i;
        }
        System.out.println(product);
    }
}
