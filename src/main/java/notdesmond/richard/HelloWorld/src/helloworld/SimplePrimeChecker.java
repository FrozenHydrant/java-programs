package notdesmond.richard.HelloWorld.src.helloworld;

import java.util.Scanner;

public class SimplePrimeChecker {
    public static void main(String args[]) {
        Scanner userInput = new Scanner(System.in);
        int userInteger = Integer.parseInt(userInput.nextLine());
        userInput.close();

        boolean isPrime = true;
        for (int i = 2; i < userInteger; i++) {
            if (userInteger % i == 0) {
                isPrime = false;
                break;
            }
        }
        if (isPrime) {
            System.out.println(userInteger + " is a prime number!");
        }
        else {
            System.out.println(userInteger + " is not a prime number!");
        }
    }
}