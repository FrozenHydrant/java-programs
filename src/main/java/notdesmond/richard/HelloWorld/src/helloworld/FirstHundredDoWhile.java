package notdesmond.richard.HelloWorld.src.helloworld;

public class FirstHundredDoWhile {
    public static void main(String args[]) {
        int sum = 0;
        int i = 1;
        do {
            sum = sum + i;
            i = i + 1;
        } while (i <= 100);
        System.out.println(sum);
    }

}