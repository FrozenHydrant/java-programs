/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notdesmond.richard.HelloWorld.src.helloworld;

/**
 *
 * @author ricky
 */
public class ForLoopBeginning {

    public static void main(String[] args) {
        int sum = 0;
        for (int i = 5; i <= 55; i++) {
            sum = sum + i;
            System.out.println("Sum: " + sum + ", i: " + i);
            System.out.println("*******************************");
        }
        System.out.println("Final sum: " + sum);
        System.out.println("\n");
        System.out.println("NEXT: ---------------------------------");
        System.out.println("\n");
        sum = 0;
        for (int i = 2; i <= 100; i = i + 2) {
            sum = sum + i;
            System.out.println("Sum: " + sum + ", i: " + i);
            System.out.println("*******************************");
        }
        System.out.println("Final sum: " + sum);
    }
}
