package desmond.richard.twentymultiplicationtable;

public class LegitTwentyMultiplicationTable {
    public static void main(String args[]) {
        String amperes = "0102030405060708091011121314151617181920";
        String coulombs = "0102030405060708091011121314151617181920";

        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                System.out.print(Integer.parseInt(Character.toString(amperes.charAt(j * 2)) + Character.toString(amperes.charAt(j * 2 + 1))) * Integer.parseInt(Character.toString(coulombs.charAt(i * 2)) + Character.toString(coulombs.charAt(i * 2 + 1))) + " ");
            }
            System.out.println("");
        }
    }
}