package desmond.richard.twentymultiplicationtable;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class TwentyMultiplicationTable {
    public static void main(String args[]) {
        try {
            FileReader fr = new FileReader("twentyMultiplicationTable.txt");
            BufferedReader br = new BufferedReader(fr);
            String readerIn;
            do {
                readerIn = br.readLine();
                if (readerIn != null) {
                    System.out.println(readerIn);
                }
            } while (readerIn != null);
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}