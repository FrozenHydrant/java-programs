package desmond.richard.bitmapflip;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class BitmapFlip {
    public static void main(String[] args) throws IOException {
        Scanner userInput = new Scanner(System.in);
        FileReader fr = new FileReader(userInput.nextLine());
        userInput.close();
        BufferedReader br = new BufferedReader(fr);
       String scannedFromFile;
        do {
            scannedFromFile = br.readLine();
            if (scannedFromFile != null) {
                System.out.println(scannedFromFile);
            }
        } while (scannedFromFile != null);

        br.close(); 
    }
}