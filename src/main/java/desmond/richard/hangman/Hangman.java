package desmond.richard.hangman;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Hangman {
    public static void main(String args[]) {
        boolean shouldLoseLife = true;
        Scanner userInput = new Scanner(System.in);
        System.out.println("Choose Difficulty: Hard, Insane, Impossible");
        String lifeChoices = userInput.nextLine();
        int lives = 0;
        if (lifeChoices.equals("Hard")){
            lives = 6;
        } else if (lifeChoices.equals("Insane")) {
            lives = 4;
        } else if (lifeChoices.equals("Impossible")) {
            lives = 2;
        }
        ArrayList<String> userGuess = new ArrayList<String>();
        String hiddenWord;
        char userAnswer;
        Random rn = new Random();
        ArrayList<String> words = new ArrayList<String>();

        try {
            FileReader fr = new FileReader("words.txt");
            BufferedReader br = new BufferedReader(fr);
            String scannedFromFile;
            do {
                scannedFromFile = br.readLine();
                if (scannedFromFile != null) {
                    words.add(scannedFromFile.toUpperCase());
                }
            } while (scannedFromFile != null);
            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        hiddenWord = words.get(rn.nextInt(190));
        for (int i = 0; i < hiddenWord.length(); i++) {
            if (hiddenWord.charAt(i) == " ".charAt(0)) {
                userGuess.add(" ");
            } else {
                userGuess.add("_");
            }
        }
        while (true) {
            shouldLoseLife = true;
            userAnswer = (userInput.nextLine().toUpperCase()).charAt(0);
            for (int i = 0; i < hiddenWord.length(); i++) {
                if (hiddenWord.charAt(i) == userAnswer) {
                    userGuess.remove(i);
                    userGuess.add(i, Character.toString(userAnswer));
                    shouldLoseLife = false;
                }
            }
            if (shouldLoseLife) {
                lives -= 1;
                System.out.println("You have " + lives + " lives left.");
                if (lives < 1) {
                    System.out.println("Game Over! The word was " + hiddenWord);
                    break;
                }
            }
            System.out.println(userGuess);
        }
        userInput.close();
    }
}