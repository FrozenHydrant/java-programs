package desmond.richard.chap1;

import java.util.Scanner;

public class PrimeNumberChecker {
    public static void main(String[] args) {
        System.out.print("Please enter a number to check:");
        Scanner input = new Scanner(System.in);
        String userInput = input.nextLine();
        int userNumber = Integer.parseInt(userInput);
        input.close();
        long startTime = System.nanoTime();
        boolean isPrime = true;
        int divisor = 0;

        for (int i = 2; i < userNumber; i++) {
            if (userNumber % i == 0) {
                isPrime = false;
                divisor = i;
                break;
            }
        }

        if (isPrime == false) {
            System.out.println("The number is not a prime. It's divisible by something, to be sure. Specifically, "
                    + divisor + " and " + userNumber / divisor + ".");
        } else {
            System.out.println("Your number is a prime!");
        }

        long endTime = System.nanoTime();
        long duration = (endTime - startTime);
        System.out.println("It took me " + duration + " nanoseconds to figure that out.");
    }
}