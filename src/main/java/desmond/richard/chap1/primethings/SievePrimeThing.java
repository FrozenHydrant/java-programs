package desmond.richard.chap1.primethings;

public class SievePrimeThing {
    public static void main(String[] args) 
    {
        final int MAX = 1000000;
        boolean[] allPrimes = new boolean[MAX];
        int amountOfPrimes = 0;
        for (int i = 3; i < 1001; i++) {
            for (int j = 0; j < MAX; j++) {
                if ((j + 1) % i == 0 && allPrimes[j] == false) {
                    allPrimes[j] = true;
                }
            }
            System.out.println("Number " + i + " completed and crossed out.");
        }
        for (int f = 0; f < MAX; f++) {
            if (allPrimes[f] == false) {
                amountOfPrimes += 1;
            }
        }
        System.out.println("We found that there are " + amountOfPrimes + " primes from 0 to 1 million.");
    }
}
