package desmond.richard.chap1;

import java.util.ArrayList;
import java.util.Scanner;

public class FactorsOfNumber {
    public static void main(String[] args) {
        System.out.println("Enter an integer.");
        Scanner userInput = new Scanner(System.in);
        int userNumber = Integer.parseInt(userInput.nextLine());
        userInput.close();

        ArrayList<Integer> factors = new ArrayList<Integer>();
        ArrayList<Integer> finalFactors = new ArrayList<Integer>();
        for (int i = 2; i <= userNumber; i++) {
            if (userNumber % i == 0) {
                factors.add(i);
            }
        }

        for (int i = 0; i < factors.size(); i++) {
            if (CheckPrimeNumber.CheckIntPrime(factors.get(i)) && finalFactors.contains(factors.get(i)) != true) {
                finalFactors.add(factors.get(i));
            } else {

            }
        }

        System.out.println("These are the prime factors of: " + userNumber);
        for (int i = 0; i < finalFactors.size(); i++) {
            System.out.println(finalFactors.get(i));
        }
    }

}