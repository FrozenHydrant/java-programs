package desmond.richard.chap1;

public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello World!"); /*
                                             * This is a comment which stretches multiple lines. See? Look this is code:
                                             * int Simon = 97
                                             */
    }
}