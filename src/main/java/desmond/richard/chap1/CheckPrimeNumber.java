package desmond.richard.chap1;

public class CheckPrimeNumber {
    public static boolean CheckIntPrime(Integer input) {
        for (int i = 2; i < input; i++) {
            if (input % i == 0) {
                return false;
            }
        }
        return true;
    }
}