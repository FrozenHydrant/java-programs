package desmond.richard.chap1;

import java.lang.Math;
import java.util.Scanner;

public class BinaryToDecimal {
    public static void main(String[] args) {
        System.out.print("Please enter a binary number:");
        Scanner input = new Scanner(System.in);
        String userNumber = input.nextLine();
        input.close();
        long startTime = System.nanoTime();
        float finalAnswer = 0;
        for (int i = 0; i < userNumber.length(); i++) {
            finalAnswer += Integer.parseInt(Character.toString(userNumber.charAt(userNumber.length() - 1 - i)))
                    * Math.pow(2, i);
        }
        System.out.println(
                "The binary number " + userNumber + " as a decimal number is " + Math.round(finalAnswer) + ".");
        long endTime = System.nanoTime();
        long duration = endTime - startTime;
        System.out.println("It took me " + duration + " nanoseconds to figure that out.");
    }
}