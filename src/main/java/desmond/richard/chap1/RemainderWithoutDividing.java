package desmond.richard.chap1;

import java.util.Scanner;

public class RemainderWithoutDividing {
    public static void main(String args[]) {
        System.out.println("Enter the number to be divided:");
        Scanner userInput = new Scanner(System.in);
        int userInteger = Integer.parseInt(userInput.nextLine());
        System.out.println("Enter the number to divide by:");
        int userIntegerDivisor = Integer.parseInt(userInput.nextLine());
        int newUserInteger = userInteger;
        userInput.close();
        do {
            newUserInteger = newUserInteger - userIntegerDivisor;
        } while (newUserInteger - userIntegerDivisor > 0);
        System.out.println("The remainder of " + userInteger + " and " + userIntegerDivisor + " is " + newUserInteger);
    }
}