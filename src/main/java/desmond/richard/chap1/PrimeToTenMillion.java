package desmond.richard.chap1;

import java.util.ArrayList;

public class PrimeToTenMillion {
    public static void main(String args[]) {
        ArrayList<Integer> storedPrimes = new ArrayList<Integer>();
        boolean isPrime;
        System.out.println(2);
        storedPrimes.add(2);
        for (int i = 3; i < 10000; i++) {
            isPrime = true;
            for (int j = 0; j < storedPrimes.size(); j++) {
                if (i % storedPrimes.get(j) == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) {
                System.out.println(i);
                storedPrimes.add(i);
            }
        }
    }
}