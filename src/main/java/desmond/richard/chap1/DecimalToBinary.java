package desmond.richard.chap1;

import java.util.Scanner;

public class DecimalToBinary {
    public static void main(String[] args) {
        System.out.print("Please enter a decimal number:");
        Scanner input = new Scanner(System.in);
        String userInput = input.nextLine();
        int userNumber = Integer.parseInt(userInput);
        input.close();
        long startTime = System.nanoTime();
        System.out.println(
                "The decimal number " + userNumber + " in binary is " + Integer.toBinaryString(userNumber) + ".");
        long endTime = System.nanoTime();
        System.out.println("It took me " + (endTime - startTime) + " nanoseconds to figure that out.");
    }
}