package desmond.richard.chap1;

import java.util.Scanner;

public class FlipNameBackwards {
    public static void main(String args[]) {
        System.out.println("Enter a string");
        Scanner userInput = new Scanner(System.in);
        char tempVar;
        String toFlip = userInput.nextLine();
        userInput.close();
        char[] arrayOfChars = new char[toFlip.length()];
        for (int i = 0; i < toFlip.length(); i++) {
            arrayOfChars[i] = toFlip.charAt(i);
        }
        System.out.println(arrayOfChars);
        for (int i = 0; i < arrayOfChars.length / 2; i++) {
            tempVar = arrayOfChars[i];
            arrayOfChars[i] = arrayOfChars[arrayOfChars.length-1-i];
            arrayOfChars[arrayOfChars.length-1-i] = tempVar;

        }
        System.out.println(arrayOfChars);
        
        
    }
}