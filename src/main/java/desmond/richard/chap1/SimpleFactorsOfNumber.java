package desmond.richard.chap1;

import java.util.Scanner;

public class SimpleFactorsOfNumber {
    public static void main(String args[]) {
        System.out.println("Enter an integer.");
        Scanner userInput = new Scanner(System.in);
        int userNumber = Integer.parseInt(userInput.nextLine());
        userInput.close();

        System.out.println("These are the prime factors of" + userNumber);
        for (int i = 2; i < userNumber; i++) {
            if (userNumber % i == 0) {
                if (CheckPrimeNumber.CheckIntPrime(i)) {
                    System.out.println(i);
                }
            }
        }
    }
}