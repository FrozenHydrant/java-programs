package desmond.richard.chap1.unixtime;

public class UnixTimestampTime {
    public static void main(String args[]) {
        String[] monthNames = new String[]{"Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"};
        long currentUnixTimeMilliseconds =  System.currentTimeMillis();
        int daysSinceUnix;
        int hoursSinceUnix;
        int minutesSinceUnix;
        int secondsSinceUnix;
        int currentYear = 1970;
        int currentMonth = 1;
        String currentMonthString;

        final int MILLISECONDS_DAY = 86400000;
        final int MILLISECONDS_HOUR = 3600000;
        final int MILLISECONDS_MINUTE = 60000;
        final int MILLISECONDS_SECOND = 1000;
        System.out.printf("UNIX time in milliseconds: %s\n", currentUnixTimeMilliseconds);
        daysSinceUnix = (int) Math.floor(currentUnixTimeMilliseconds / MILLISECONDS_DAY);
        currentUnixTimeMilliseconds = currentUnixTimeMilliseconds % MILLISECONDS_DAY;
        hoursSinceUnix = (int) Math.floor(currentUnixTimeMilliseconds / MILLISECONDS_HOUR);
        currentUnixTimeMilliseconds = currentUnixTimeMilliseconds % MILLISECONDS_HOUR;
        minutesSinceUnix = (int) Math.floor(currentUnixTimeMilliseconds / MILLISECONDS_MINUTE);
        currentUnixTimeMilliseconds = currentUnixTimeMilliseconds % MILLISECONDS_MINUTE;
        secondsSinceUnix = (int) Math.floor(currentUnixTimeMilliseconds / MILLISECONDS_SECOND);
        currentUnixTimeMilliseconds = currentUnixTimeMilliseconds % MILLISECONDS_SECOND;
        for (int i = 1; i > 0; i++) {
            if (currentYear % 4 == 0) {
                if (daysSinceUnix < 367) {
                    break;
                }
                daysSinceUnix -= 366;
            } else {
                if (daysSinceUnix < 366) {
                    break;
                }
                daysSinceUnix -= 365;
            }
            currentYear += 1;

        }
        int untouchedDaysSinceUnix = daysSinceUnix;
        daysSinceUnix = UnixTimeCalculateDays.CalculateDays(untouchedDaysSinceUnix, currentYear, currentMonth);
        currentMonth = UnixTimeCalculateMonth.CalculateMonth(untouchedDaysSinceUnix, currentYear, currentMonth);
        currentMonthString = monthNames[currentMonth - 1];
        System.out.printf("The current time is %s %d, %d %02d:%02d:%02d UTC and %d milliseconds.", currentMonthString, daysSinceUnix, currentYear, hoursSinceUnix, minutesSinceUnix, secondsSinceUnix, currentUnixTimeMilliseconds);
    }
}