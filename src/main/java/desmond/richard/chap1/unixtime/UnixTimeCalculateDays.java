package desmond.richard.chap1.unixtime;

public class UnixTimeCalculateDays {
    public static int CalculateDays(int daysSinceUnix, int currentYear, int currentMonth) {
        for (int i = 1; i > 0; i++) {
            if (currentMonth == 1 || currentMonth == 3 || currentMonth == 5 || currentMonth == 7 || currentMonth == 8
                    || currentMonth == 10 || currentMonth == 12) {
                if (daysSinceUnix < 32) {
                    return daysSinceUnix;
                }
                daysSinceUnix -= 31;
            } else if (currentMonth == 4 || currentMonth == 6 || currentMonth == 9 || currentMonth == 11) {
                if (daysSinceUnix < 31) {
                    return daysSinceUnix;
                }
                daysSinceUnix -= 30;
            } else if (currentMonth == 2) {
                if (currentYear % 4 == 0) {
                    if (daysSinceUnix < 30) {
                        return daysSinceUnix;
                    }
                    daysSinceUnix -= 29;
                } else {
                    if (daysSinceUnix < 29) {
                        return daysSinceUnix;
                    }
                    daysSinceUnix -= 28;
                }

            }
            currentMonth += 1;
        }
        return 23232;
    }
}