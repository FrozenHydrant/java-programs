package desmond.richard;

public class CharWordSumGoodStrings {

    public static void main(String args[]) {

        String[] words = { "hello", "world", "leetcode" };
        String[] wordComparison = new String[words.length];
        int lengthCount = 0;
        for (int i = 0; i < words.length; i++) {
            wordComparison[i] = words[i];
        }
        String chars = "welldonehoneyr";
        for (int i = 0; i < chars.length(); i++) {
            for (int j = 0; j < words.length; j++) {
                for (int k = 0; k < words[j].length(); k++) {
                    if (words[j].charAt(k) == chars.charAt(i)) {
                        words[j] = words[j].substring(0, k) + words[j].substring(k+1);
                    }
                }
            }
        }
        for (int j = 0; j < words.length; j++) {
            if (words[j].length() == 0) {
                lengthCount += wordComparison[j].length();
            }
        }
        System.out.println(lengthCount);
    }
}