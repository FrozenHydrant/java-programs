package desmond.richard.guessthenumber;

import java.util.Random;
import java.util.Scanner;

import desmond.richard.functions.FunctionReturnBiggerNumber;

public class GuessTheNumber {
    public static void main(String args[]) {
        Random rn = new Random();
        int targetNumber = rn.nextInt(100);
        Scanner input = new Scanner(System.in);
        int userPatheticGuess = 0;
        do {
            System.out.println("Guess a number between 1 and 100!");
            try {
                userPatheticGuess = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("That was not a number.");
            }
            if (0 < userPatheticGuess && userPatheticGuess < 101) {
                if (FunctionReturnBiggerNumber.LargerNumber(userPatheticGuess, targetNumber) == userPatheticGuess
                        && targetNumber != userPatheticGuess) {
                    System.out.println("Your guess was too large!");
                } else if (FunctionReturnBiggerNumber.LargerNumber(userPatheticGuess, targetNumber) == targetNumber
                        && targetNumber != userPatheticGuess) {
                    System.out.println("Your guess was too small!");
                }
            } else {
                System.out.println("The number entered is not within the bounds specified. Please try again.");
            }
        } while (userPatheticGuess != targetNumber);
        System.out.println("Gotem!");
        input.close();
    }
}